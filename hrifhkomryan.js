#!/usr/bin/env node
'use strict';
const fs = require('fs');
const file = process.argv[2];

const kyittlat = "zi";
const kyittwip = "zi.kyit.";
const hniktlat = "ti";
const kyittwap = ".kyit.zi";
const nyitlwat = ["do","po","tl6h","wo", "gi"];
const gvaklwat = ["ka","na","ma","ca","vi"];
const htinhzotlwat = ["li","tu","ri","si","psih","rwih","ksuh"];
const hyantlat = "la";
const tyaftlat = "prah";

const hnuckwon = {"zron":0, "hyik": 1, "tyut": 2, "tyin": 3, "ksas": 4, "hfak": 4};

const gvakkwon = {"ka":"hkak", "ma":"htac", "na":"hnak", "ca": "hcan"};

function tlatpukahnuctu(tlatpu) {
  return tlatpu.reduce((hnuc, tlat, htik) => {
    const hbis = Math.pow(6, htik);
    return hnuc + hnuckwon[tlat] * hbis;
  }, 0);
  
}

function fles(hwus) {
  let psas = hwus.split(/[ \n]/)
  psas = psas.filter((tlat) => {
    return tlat.length > 0;
  });
  // split based on sentence ending, make list of sentences
  let fyak = [];
  let psaslwat = [];
  let nyitpsut = {};
  let fyakpsut = {};
  let htinpsut = {};
  psas.forEach((tlat) => {
    if (tlat.localeCompare(tyaftlat) == 0) {
      psaslwat.push(tlat);
    } else if (nyitlwat.includes(tlat)) {
      if (tlat.localeCompare("do") == 0) {
        let hnuc = tlatpukahnuctu(fyak);
        fyak = {};
        fyak.do = hnuc;
      } else if (tlat.localeCompare("gi") == 0) {
        let hnim = fyak.join("");
        fyak = {};
        fyak.gi = hnim;
      } else {
        let prif = fyak;
        fyak = {};
        fyak[tlat] = prif;
        //nyitpsut[tlat] = fyak;
        //fyak = [];
        //fyak.push(nyitpsut);
      }
    } else if (gvaklwat.includes(tlat)) {
      fyakpsut[tlat] = fyak;
      fyak = [];
    } else if (htinhzotlwat.includes(tlat)) {
      if (fyak.length > 0) { fyakpsut["vi"] = fyak;}

      htinpsut[tlat] = fyakpsut;
      fyak = [];
      fyakpsut = {};
      psaslwat.push(htinpsut);
      htinpsut = {};
    } else {
      if (fyak) {
      fyak.push(tlat);
      }
    }
  });
  return psaslwat;

}

function dluthnimmahtinpsutkaryantu(htinpsut) {
  let dluthnim = htinpsut.vi;
  delete htinpsut.vi;
  dluthnim = Object.keys(htinpsut).map((gvak) => {
          let nyittlat = Object.keys(htinpsut[gvak])? 
            Object.keys(htinpsut[gvak]).join(""): "";
          return nyittlat + gvak; }).join("") + dluthnim;
  return dluthnim;
}

function hwusmahtinpsutkaryantu(htinpsut) {
  let psas = "{";
  Object.keys(htinpsut).forEach((gvak, htik) => {
    if (htik > 0 ) psas += ", ";
    if (htinpsut[gvak].gi) {
      psas += `"${gvakkwon[gvak]}":${htinpsut[gvak].gi}`;
    } else {
      psas += `"${gvakkwon[gvak]}":${JSON.stringify(htinpsut[gvak])}`;
    }

  });
  psas += "}"
  return psas;
}
function ryan(pyacpsutlwat) {
  let psas = "";
  psas += "include <stdio.h>\n";
  psas += `int main() {\n`;
  psas += `function prifhfan(prif) { if (prif.gi) return prifhfan(prif.gi); return prif; }\n`
  psas += `function gimadokaplus ({htac, hkak}) { this[htac] += hkak.do}\n`;
  psas += `function wokahsac({hkak}) { console.log(hkak.wo.join(" ")); }\n`;
  psas += `function gikahsac({hkak}) { console.log(hkak); }\n`;
  pyacpsutlwat.forEach((pyacpsut) => {
    // realis sentence
    if (typeof pyacpsut  === "string" && pyacpsut.localeCompare(tyaftlat) == 0) {
      psas += "}\n";
    } else if (pyacpsut.li != undefined) {
      let htinpsut = pyacpsut.li;
      if (htinpsut.na && htinpsut.na.gi) {
        psas += `${htinpsut.na.gi} = `;
        delete htinpsut.na;
        if (htinpsut.ka && htinpsut.ka.do) {
          psas += JSON.stringify(htinpsut.ka.do);
        }
        delete htinpsut.ka;
        if (Object.keys(htinpsut).length > 0 ) {
          psas += `\/* ${JSON.stringify(htinpsut)} *\/;\n`;
        } else {
          psas += ";\n";
        }
      } else {
        psas += `\/* ${JSON.stringify(htinpsut)} *\/;\n`;
      }
    } else if (pyacpsut.ksuh != undefined ) /* declarative sentence */ {
      let htinpsut = pyacpsut.ksuh;
      let dluthnim = dluthnimmahtinpsutkaryantu(htinpsut);
      psas += `\nfunction ${dluthnim}`;
      if (Object.keys(htinpsut).length == 0) {
        psas += `() {\n`;
      } else {
        let nruplwat = Object.keys(htinpsut).map((gvak) => 
          { return  gvakkwon[gvak];
          }).join(", ");
        psas += `({${nruplwat}}) {\n`;
      }
    } else if (pyacpsut.tu != undefined) {
      let htinpsut = pyacpsut.tu;
      let dluthnim = dluthnimmahtinpsutkaryantu(htinpsut);
      psas += `${dluthnim}`;
      if (Object.keys(htinpsut).length == 0) {
        psas += `();\n`;
      } else {
        let nruppsut = hwusmahtinpsutkaryantu(htinpsut);
        psas += `(${nruppsut});\n`;
      }
    } else {
      psas +=`/* ${JSON.stringify(pyacpsut)} */`;
    }
  });
  psas += `return 0; }`
  return psas;
}

fs.readFile(file, "utf8", function(err, pyachwus) { 
  let hlaspu = pyachwus.split("\n");
  let pyacpsutlwat = fles(pyachwus);
  //console.log(JSON.stringify(pyacpsutlwat));
  process.stdout.write(ryan(pyacpsutlwat));
});


