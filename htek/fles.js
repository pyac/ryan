"use strict";
const pyac = require("./pyac.js");
Object.assign(global, pyac);


function hkantsen(fyak) {
  if (fyak[0] == "hsic") fyak[0] = ".";
  fyak = fyak.map((tlat) => {
    if (tlat == "ti") return "/";
    return tlat;
  });
  return fyak.join("");
}
function nyafslismahwuskaflistu(hwus) {
  // kyitka krettu
  let psas = kyitkakrettu(hwus);
  //let psas = hwus.split(/tlatdrec/)
  psas = psas.filter((tlat) => {
    return tlat.length > 0;
  });
  return psas;
}

function kyitkakrettu(hwus) {
  let psas = [];
  let kyit = kyittlep(hwus);
  let twichwus = hwus.slice(0);
  while(kyit) {
    let twip = twichwus.indexOf(kyit);
    let lyan = kyit.length;
    psas = psas.concat(twichwus.slice(0, twip).split(tlatdrec));
    psas.push(kyit);
    twichwus = twichwus.slice(twip+lyan);
    kyit = kyittlep(twichwus);
  };
    psas = psas.concat(twichwus.split(tlatdrec));
  return psas;
}

function kyittlep(hwus) {
 let htik = hwus.indexOf("zi.");
 if (htik == -1) return null;
 let twip = hwus.slice(htik);
 let kyittlat = twip.match(/\.[a-z0-9]*\./)[0];
 let twap = twip.slice(2+kyittlat.length);
 htik = twap.indexOf(kyittlat + "zi");
 let kyit = twip.slice(0,2*2+kyittlat.length*2+htik);
 return kyit;
}
function cwectsen(fyakpsut, fyak) {
  fyak = fyak.map((tlat) => {
    return /^ti$/.test(tlat)? "." : tlat;
  });
  let cwec = fyak.filter((tlat) => {
    return hgaftlatri(tlat);
  });
  const kres = fyak.filter((tlat) => {
    return ! hgaftlatri(tlat);
  });
  if (kres.length > 0) {
    fyakpsut[krestlat] = [kres.join("")];
  }
  if (cwec.length > 0) {
    if (fyakpsut[cwectlat]) {
      cwec = cwec.concat(fyakpsut[cwectlat]);
    }
    fyakpsut[cwectlat] = cwec;
  }
  return fyakpsut;
}
function hgaftlatri(tlat) {
  return /[h27aeiou63]$/.test(tlat);
}

function hniktsen(fyak, hnikpsut) {
  let psashwus = new String();
  let psaspsut = new Object();
  //console.log("hnikpsutvi " +JSON.stringify(hnikpsut));
  //console.log("fyakvi " + JSON.stringify(fyak));
  //if (fyak.wo) {
  //  psashwus = fyak.wo;
  //  fyak = psashwus;
  //} else if (fyak.length > 0) {
  if (fyak.length > 0) {
    psashwus = fyak.join("");
    fyak = psashwus;
  }
  psaspsut = fyak;
  return psaspsut;
}
function slistsen(fyak, slispsut) {
  let psashwus = new String();
  let psaspsut = new Object();
  //console.log("slispsutvi " +JSON.stringify(slispsut));
  //console.log("fyakvi " + JSON.stringify(fyak));
  if (fyak.wo) {
    psaspsut = fyak.wo;
    fyak = psaspsut;
  } else if (fyak.length > 0) {
    psaspsut = fyak.join("");
    fyak = psaspsut;
  } else {
    psaspsut = fyak;
  }
    //console.log("psaspsut " + JSON.stringify(psaspsut));
  return psaspsut;
}

function tlatpukahnuctu(tlatpu) {
  let hbis = hnuchbis;
  let hbishtik = 0;
  return tlatpu.reduce((hnuc, tlat, htik) => {
    let psas = 0;
    if (tlat.localeCompare(hbistlat) == 0) {
      hbis = hnuc;
      hbishtik = htik + 1;
      psas = 0;
    } else {
      const hbispsam  = Math.pow(hbis, htik - hbishtik);
      psas = hnuc + hnuckwon[tlat] * hbispsam;
    }
    return psas;
  }, 0);
}

exports.fles = function (hwus) {
  let psas = nyafslismahwuskaflistu(hwus);
  // split based on sentence ending, make list of sentences
  let fyak = [];
  let psaslwat = [];
  let nyitpsut = {};
  let fyaklwat = [];
  let fyakpsut = {};
  let htinpsut = {};
  let hnikpsut = {};
  let hniklwat = [];
  let slispsut = {};
  let slislwat = [];
  let hyanpsut = {};
  psas.forEach((tlat) => {
     if (tlat.localeCompare(tyaftlat) == 0) {
      psaslwat.push(tlat);
    } else if (nyitlwat.includes(tlat)) {
      if (tlat.localeCompare("do") == 0) {
        let hnuc = tlatpukahnuctu(fyak);
        fyak = [];
        nyitpsut = {};
        nyitpsut.do = hnuc;
        fyak = nyitpsut;
      } else if (tlat.localeCompare("gi") == 0) {
        let hnim = fyak.join("");
        fyak = [];
        nyitpsut = {};
        nyitpsut.gi = hnim;
        fyak = nyitpsut;
      } else if (tlat.localeCompare("wo") == 0) {
        let hsictlat = fyak.join("");
        fyak = [];
        nyitpsut = {};
        const syactlat = hsictlat.replace(/^\./,"").replace(/\.$/,"");
        nyitpsut["wo"] = syactlat;
        fyak = nyitpsut;
      } else if (tlat.localeCompare("r6") == 0) {
        let hnim = fyak.join("")+"()";
        fyak = [];
        nyitpsut = {};
        nyitpsut.gi = hnim;
        fyak = nyitpsut;
      } else if (tlat.localeCompare("to") == 0) {
        let hkanhnim = hkantsen(fyak);
        fyak = [];
        nyitpsut = {};
        nyitpsut.to = hkanhnim;
        fyak = nyitpsut;
      } else {
        let prif = fyak;
        nyitpsut = {};
        nyitpsut[tlat] = prif;
        fyak = nyitpsut;
      }
    } else if (kyittlep(tlat)) {
      const kyittwiptlep = new RegExp(kyittwiptlat);
      const kyittwiptlatlyan = tlat.match(kyittwiptlep)[0].length;
      nyitpsut = {};
      const kyit = tlat.slice(kyittwiptlatlyan,-kyittwiptlatlyan);;
      nyitpsut.zi = kyit;
      //fyaklwat.push(nyitpsut);
      fyak = [];
      fyak = nyitpsut;
    } else if (tlat.localeCompare(hcuttlat) == 0) {
      if (fyak.length > 0) {fyakpsut = cwectsen(fyakpsut, fyak); }
      htinpsut[tlat] = fyakpsut;
      fyakpsut = {};
      hyanpsut = {};
      fyak = [];
    } else if (tlat.localeCompare(hyantlat) == 0) {
      if (fyak.length > 0) { fyakpsut = cwectsen(fyakpsut, fyak);}
      hyanpsut[tlat] = fyakpsut;
      fyak = hyanpsut;
      fyakpsut = {};
    } else if (tlat.localeCompare("ti") == 0) {
      let hnik = hniktsen(fyak, hnikpsut);
      fyak = [];
      hniklwat.push(hnik);
    } else if (tlat.localeCompare("swoh") == 0) {
      let slishcen = slistsen(fyak, slispsut);
      fyak = [];
      //console.log("slishcen " + JSON.stringify(slishcen));
      slislwat.push(slishcen);
    } else if (gvaklwat.includes(tlat)) {
      if (fyak.length > 0) {
        fyak = [fyak.join("")];
      }
      if (hniklwat.length > 0 ) {
        const hnik = hniktsen(fyak);
        fyak = hniklwat.concat(hnik);
        hniklwat = [];
      }
      if (slislwat.length > 0 ) {
        const hcen = slistsen(fyak);
        fyak = {"swoh":slislwat.concat(hcen)};
        //console.log("fyak " + JSON.stringify(fyak));
        slislwat = [];
      }
      fyakpsut[tlat] = fyak;
      fyak = [];
    } else if (vriklwat.includes(tlat)) {
      if (fyak.length > 0) { fyakpsut = cwectsen(fyakpsut, fyak);}
      if (hniklwat.length > 0 ) {
        fyakpsut.vi = hniklwat.concat(fyakpsut.vi);;
      }
      htinpsut[tlat] = fyakpsut;
      fyak = [];
      fyakpsut = {};
      psaslwat.push(htinpsut);
      //console.log(JSON.stringify(htinpsut));
      htinpsut = {};
      hniklwat = [];
      hyanpsut = {};
    } else {
      if (fyak) {
      //console.log("crocvi" +JSON.stringify(fyak));
      fyak.push(tlat);
      }
    }
  });
  return psaslwat;

}
