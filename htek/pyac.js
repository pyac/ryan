exports.hnuchbis = 6;
exports.pyif = undefined;
exports.hcuttlat = "cu";
exports.cwectlat = "be";
exports.krestlat = "vi";
exports.pcaptlat = "pe";
exports.tlictlat = "fe";
exports.hliptlat = "mweh";
exports.hleptlat = "tfeh";
exports.hniktlat = "ti";
exports.hcennyitlwat = ["slis","hwus","hnuc","psut"];
exports.tlatdrec = new RegExp("[ \n\t]");
exports.kyittlat = "zi";
exports.kyittwiptlat = "zi\.[a-z0-9]*\.";
exports.kyittwaptlat = ".kyit.zi";
exports.nyitlwat = ["do","po","tl6h","wo", "gi", "to", "r6", "sreh"];
exports.gvaklwat = ["ca","wu","ta","na","so","yu","lwoh","nweh","ma","lweh","pwah","tloh","de","ka","vi","be"];
exports.vriklwat = ["li","tu","ri","si","psih","rwih","ksuh","fi","mwih","pi","tcih"];
exports.hyantlat = "la";
exports.hkuktlat = "ko";
exports.fraktlat = "frak";
exports.ksoctlat = "ksoc";
exports.jractlat = "jrac";
exports.hbistlat = "brih";
exports.tyaftlat = "prah";
exports.hwantlat = "wa";
exports.hnunhlaslyat = "\n";
exports.hnamtlat = "ba";
exports.rwoctlat = "swoh";
exports.hn6tlwat = ["wa","ba","swoh"];
exports.hnuckwon = {"zron":0, "hyik": 1, "tyut": 2, "tyin": 3, "ksas": 4, 
  "hfak": 5, "hlis":6, "hsip":7, "hwap": 8, "twun": 9, "htip": 10, "slen": 11,
  "tfat": 12, "tses":13, "hses":14, "hpet": 15, "hsos": 16, "sret": 17, 
  "hdap": 18, "dzin": 19};
exports.gvakkwon = {"ka":"hkak", "ma":"htac", "lweh":"lwik", "lwoh":"kwek",
  "nweh":"nwik", "pwah":"hpak", "na":"hnak", "ca": "hcan", "so":"hsuk", 
  "tloh":"tlik", "be": "cwec", "vi":"kres", "yu":"hyak", "de":"hfik" };
exports.nyitkwon = {"do":"hnuc","po":"pluc","tl6h":"tlom","wo":"tlat", "gi":"hnim",
  "to":"hkan", "r6":"pfas", "sreh":"slis"};
