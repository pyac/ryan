
const hnuckwon = {"zron":0, "0":"zron",
  "hyik": 1, "1":"hyik",
  "tyut": 2, "2":"tyut",
  "tyin": 3, "3":"tyin",
  "ksas": 4, "4":"ksas",
  "hfak": 5, "5":"hfak",
  "hlis":6, "6":"hlis",
  "hsip":7, "7":"hsip",
  "hwap": 8, "8":"hwap",
  "twun": 9, "9":"twun",
  "htip": 10, "10":"htip",
  "slen": 11, "11":"slen",
  "tfat": 12, "12":"tfat",
  "tses":13, "13":"tses",
  "hses":14, "14":"hses",
  "hpet": 15, "15":"hpet",
  "hsos": 16, "16":"hsos",
  "sret": 17, "17":"sret",
  "hdap": 18, "18":"hdap",
  "dzin": 19, "19":"dzin"};

let hnuchbis = 6;

function tlatpumahnuckaryan(hnuc) {
  let hbis = hnuchbis;
  return hnuc.toString(6).split("").reverse().map((hnuc) => {
    return hnuckwon[hnuc];
  }).join(" ");

}

function pyacmahtinpsutkaryan(htinpsut) {
  let gwic = Object.keys(htinpsut);
  let psas = new String();
  if (typeof htinpsut == "number") {
    psas = tlatpumahnuckaryan(htinpsut);
  } else if (Array.isArray(htinpsut) && htinpsut.length > 1) {
    psas = JSON.stringify(htinpsut);
  } else if (gwic && gwic[0] != 0) {
    psas = gwic.map((tlat) => {
      if (tlat == "zi") {
        return `zi.kyit.${pyacmahtinpsutkaryan(htinpsut[tlat])}.kyit.zi`;
      } else {
        return pyacmahtinpsutkaryan(htinpsut[tlat]) + " " + tlat;
      }
    }).join(" ");
  } else {
    psas = htinpsut;
  }
  return psas;
}

function pyacmaryan(pyacpsut) {
  let psas = new String();
  if (pyacpsut.length) {
    psas = pyacpsut.map((htinpsut) => {
      return pyacmahtinpsutkaryan(htinpsut);
    });
  } else {
    psas = pyacmahtinpsutkaryan(pyacpsut);
  }
  return psas;
}
exports.pyac = function(psut) {
  return pyacmaryan(psut);
}
exports.gimalakapyac = function(psut) {
  let psas = pyacmaryan(psut.hkak.la);
  return pyacmaryan(psut.hkak.la);
}
exports.lakahsac = function ({hkak}) { console.log(pyacmaryan(hkak.la)); };
